# GoogleDogs

소켓통신을 이용한 공동 문서협업 프로그램 GoogleDogs의 프론트엔드 파트 입니다.

## Configuration
:exclamation: .env 파일을 루트 프로젝트 디렉토리에 생성후 아래의 환경변수들을 입력해야 정상 작동 합니다.

REACT_APP_FIREBASE_PROJECT_ID  
REACT_APP_FIREBASE_API_KEY  
REACT_APP_FIREBASE_SENDER_ID  
REACT_APP_FIREBASE_APP_ID  
REACT_APP_SERVER_URL(백엔드 앱을 실행하는 주소입니다.)  

## Installation

```sh
npm install
```

## Development

```sh
npm start
// localhost:3000
```
