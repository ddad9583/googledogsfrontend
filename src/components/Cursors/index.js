import styled from "styled-components";

const Cursor = styled.div.attrs((props) => ({
  style: {
    top: `calc(${props.top}px + 1rem)`,
    left: `${props.left}px`,
  },
}))`
  position: absolute;
  border: 1px dashed red;
  background-color: rgba(227, 255, 250, 0.93);
  max-width: 10rem;
  height: 1rem;
`;

const Cursors = ({ cursors, quill }) => {
  const cursorList = [];

  for (const [userId, index] of Object.entries(cursors)) {
    const adjustedIndex = index - 1 < 0 ? 0 : index - 1;
    const { top, left } = quill.getBounds(adjustedIndex);
    const newCursor = (
      <Cursor key={userId} top={top} left={left}>
        {userId}
      </Cursor>
    );

    cursorList.push(newCursor);
  }

  return cursorList;
};

export default Cursors;
