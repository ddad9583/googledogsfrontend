import {
  getAuth,
  signInWithPopup,
  GoogleAuthProvider,
  onAuthStateChanged,
} from "firebase/auth";
import { Routes, Route } from "react-router-dom";
import { useEffect, useState } from "react";

import UserProfile from "../UserProfile";
import CreateDocument from "../CreateDocument";
import Document from "../Document";

const provider = new GoogleAuthProvider();
const DUMMY_TOKEN = "DUMMY";

function App() {
  const [isAuthenticated, setIsAuthenticated] = useState(false);
  const [checkedLoggedIn, setCheckedLoggedIn] = useState(false);
  const [isError, setIsError] = useState(false);
  const [token, setToken] = useState("");
  const [userId, setUserId] = useState("");

  useEffect(() => {
    const shouldNotLoadProfile =
      !isAuthenticated && localStorage.getItem("isGoogleAuthenticated") !== "1";

    if (shouldNotLoadProfile) {
      setCheckedLoggedIn(true);
      setToken(DUMMY_TOKEN);
      return;
    }

    const auth = getAuth();

    onAuthStateChanged(auth, (user) => {
      if (user) {
        setToken(user.accessToken);
        setUserId(user.email.split("@")[0]);
        setCheckedLoggedIn(true);
        setIsAuthenticated(true);
      } else {
        setIsError(true);
        setCheckedLoggedIn(true);
        localStorage.setItem("isGoogleAuthenticated", "0");
      }
    });
  }, [token, isAuthenticated]);

  const loginWithGoogle = async () => {
    try {
      const auth = getAuth();
      const result = await signInWithPopup(auth, provider);
      const credential = GoogleAuthProvider.credentialFromResult(result);

      if (credential) {
        localStorage.setItem("isGoogleAuthenticated", "1");
        setIsAuthenticated(true);
      } else {
        setIsError(true);
      }
    } catch (error) {
      setIsError(true);
    }
  };

  let content = <h1>Checking user is Logged-in</h1>;

  if (!isAuthenticated && checkedLoggedIn) {
    content = <button onClick={loginWithGoogle}>Google login</button>;
  } else if (token !== "") {
    content = <UserProfile token={token}></UserProfile>;
  }

  return (
    <>
      <Routes>
        <Route path="/" element={content} />
        <Route path="/document">
          <Route path="create" element={<CreateDocument token={token} />} />
          <Route
            path=":documentId"
            element={<Document token={token} userId={userId} />}
          />
        </Route>
      </Routes>
      {isError && (
        <div>
          인증실패... 페이지를 새로고침 하거나 나중에 다시 시도해주세요..
        </div>
      )}
    </>
  );
}

export default App;
