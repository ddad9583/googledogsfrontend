import axios from "axios";
import { useEffect, useState } from "react";
import PropTypes from "prop-types";

import Error from "../Error";
import ProfileDetail from "./ProfileDetail";

function UserProfile({ token }) {
  const [userProfile, setUserProfile] = useState(null);
  const [isError, setIsError] = useState(false);
  const [error, setError] = useState({});

  useEffect(() => {
    const controller = new AbortController();

    (async () => {
      try {
        const response = await axios.get(
          process.env.REACT_APP_SERVER_URL + "/user",
          { headers: { Authorization: token } }
        );

        setUserProfile(response.data);
      } catch (error) {
        setIsError(true);
        setError({
          title: error.response.data.message,
          status: error.response.data.status,
          message: error.response.data.message,
        });
      }
    })();

    return () => {
      controller.abort();
    };
  }, [token]);

  return (
    <>
      {!isError && <ProfileDetail userProfile={userProfile} />}
      {isError && (
        <Error
          title={error.title}
          status={error.status}
          messge={error.message}
          link={"./"}
        />
      )}
    </>
  );
}

UserProfile.propTypes = {
  token: PropTypes.string,
};

export default UserProfile;
