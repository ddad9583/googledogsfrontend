import { Link } from "react-router-dom";
import PropTypes from "prop-types";

const ProfileDetail = ({ userProfile }) => {
  let documentList;

  if (userProfile) {
    documentList = userProfile.documents.map((document) => {
      return (
        <li key={document._id}>
          <Link to={`document/${document._id}`}>제목: {document.title}</Link>
        </li>
      );
    });
  }

  return (
    <>
      {!userProfile && <h1>Fetching profile...</h1>}
      {userProfile && (
        <>
          <Link to="document/create">새로운 문서 만들기</Link>
          <h1>환영합니다 {userProfile.email}!</h1>
          <h2>내가 만든 문서</h2>
          <ul>{documentList}</ul>
        </>
      )}
    </>
  );
};

ProfileDetail.propTypes = {
  userProfile: PropTypes.shape({
    documents: PropTypes.arrayOf(
      PropTypes.shape({
        createdBy: PropTypes.string,
        delta: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
        title: PropTypes.string,
        __v: PropTypes.number,
        _id: PropTypes.string,
      })
    ),
    email: PropTypes.string,
    __v: PropTypes.number,
    _id: PropTypes.string,
  }),
};

export default ProfileDetail;
