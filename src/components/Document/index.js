import { useCallback, useEffect, useState } from "react";
import { useParams, Link } from "react-router-dom";
import { io } from "socket.io-client";
import styled from "styled-components";
import PropTypes from "prop-types";
import Quill from "quill";
import "quill/dist/quill.snow.css";

import Cursors from "../Cursors";

const StyledLink = styled(Link)`
  display: block;
`;

const TextContainer = styled.div`
  position: relative;
  overflow: hidden;
  width: 100%;
  height: 50rem;
`;

const QuillContainer = styled.div`
  width: 100%;
  height: 50rem;
`;

const SAVE_INTERVAL = 20000;
const QUILL_ERROR_MESSAGE = "Something went wrong... Reason: ";

const Document = ({ token, userId }) => {
  const [quill, setQuill] = useState(null);
  const [documentTitle, setDocumentTitle] = useState("Loading...");
  const [socket, setSocket] = useState(null);
  const [cursors, setCursors] = useState({});
  const { documentId } = useParams();

  useEffect(() => {
    if (token === "") return;

    const socketInstance = io(process.env.REACT_APP_SERVER_URL, {
      auth: {
        token,
      },
    });

    setSocket(socketInstance);

    return () => {
      socketInstance.disconnect();
    };
  }, [token]);

  useEffect(() => {
    if (!socket || !quill) return;

    socket.once("load-document", (document) => {
      quill.setContents(document.delta);
      setDocumentTitle(document.title);
      quill.enable();
    });

    const connectErrorHandler = (error) => {
      setDocumentTitle(error.data + ": " + error.message);
      quill.setText(QUILL_ERROR_MESSAGE + error.message);
    };

    const dbErrorHandler = (error) => {
      setDocumentTitle(error.status + ": " + error.message);
      quill.setText(QUILL_ERROR_MESSAGE + error.message);
    };

    socket.on("connect_error", connectErrorHandler);
    socket.on("db-error", dbErrorHandler);
    socket.emit("get-document", documentId);

    return () => {
      socket.off("connect_error", connectErrorHandler);
      socket.off("db-error", dbErrorHandler);
    };
  }, [socket, quill, documentId]);

  useEffect(() => {
    if (!socket || !quill) return;

    const id = setInterval(() => {
      socket.emit("save-document", quill.getContents());
    }, SAVE_INTERVAL);

    return () => {
      clearInterval(id);
    };
  }, [socket, quill]);

  useEffect(() => {
    if (!socket || !quill) return;

    const handler = (delta) => {
      quill.updateContents(delta);
    };

    socket.on("receive-text-changes", handler);

    return () => {
      socket.off("receive-text-changes", handler);
    };
  }, [socket, quill]);

  useEffect(() => {
    if (!socket || !quill) return;

    const handler = (delta, oldDelta, source) => {
      if (source !== "user") return;

      const payload = { index: quill.getSelection().index, userId };

      socket.emit("send-selection-change", payload);
      socket.emit("send-text-changes", delta);
    };

    quill.on("text-change", handler);

    return () => {
      quill.off("text-change", handler);
    };
  }, [socket, quill, userId]);

  useEffect(() => {
    if (!socket || !quill) return;

    const handler = (range, oldRange, source) => {
      if (source === "user" && range) {
        const payload = { index: range.index, userId };

        socket.emit("send-selection-change", payload);
      }
    };

    quill.on("selection-change", handler);

    return () => {
      quill.off("selection-change", handler);
    };
  }, [socket, quill, userId]);

  useEffect(() => {
    if (!socket || !quill) return;

    const handler = (payload) => {
      const newCursors = { ...cursors };

      newCursors[payload.userId] = payload.index;
      setCursors(newCursors);
    };

    socket.on("receive-selection-change", handler);

    return () => {
      socket.off("receive-selection-change", handler);
    };
  }, [socket, quill, cursors]);

  const containerRef = useCallback((container) => {
    if (!container) return;

    container.innerHTML = "";

    const editor = document.createElement("div");

    container.append(editor);

    const quill = new Quill(editor, {
      theme: "snow",
      modules: {
        toolbar: false,
      },
    });

    quill.disable();
    quill.setText("Loading...");
    setQuill(quill);
  }, []);

  return (
    <>
      <StyledLink to={"/"}>메인화면으로 돌아가기</StyledLink>
      <h1>{documentTitle}</h1>
      <TextContainer>
        <QuillContainer ref={containerRef} />
        <Cursors cursors={cursors} quill={quill} />
      </TextContainer>
    </>
  );
};

Document.propTypes = {
  token: PropTypes.string,
  userId: PropTypes.string,
};

export default Document;
