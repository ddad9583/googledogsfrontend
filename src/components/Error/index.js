import { Link } from "react-router-dom";
import PropTypes from "prop-types";

const Error = ({ title, status, message, link }) => {
  return (
    <>
      <h1>{title}</h1>
      <h2>{status}</h2>
      <p>{message}</p>
      <Link to={link}>메인페이지로 돌아가기</Link>
      <button type="button" onClick={() => window.location.reload()}>
        새로 고침
      </button>
    </>
  );
};

Error.propTypes = {
  title: PropTypes.string,
  status: PropTypes.number,
  message: PropTypes.string,
  link: PropTypes.string,
};

export default Error;
