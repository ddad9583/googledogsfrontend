import { useState } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import styled from "styled-components";
import PropTypes from "prop-types";

import Error from "../Error";
import CreateResult from "./CreateResult";

const StyledLink = styled(Link)`
  display: block;
`;

const CreateDocument = ({ token }) => {
  const [isCreated, setIsCreated] = useState(false);
  const [isError, setIsError] = useState(false);
  const [error, setError] = useState({ status: null, message: null });
  const [documentURL, setDocumentURL] = useState("");
  const [enteredTitle, setDocumentTitle] = useState("");

  const formSubmissionHandler = async (event) => {
    event.preventDefault();

    try {
      const createdDocument = await axios.post(
        process.env.REACT_APP_SERVER_URL + "/document/create",
        { title: enteredTitle },
        {
          headers: { Authorization: token },
        }
      );

      setIsCreated(true);
      setDocumentURL(createdDocument.data._id);
    } catch (error) {
      setError(error.response.data);
      setIsError(true);
    }
  };

  return (
    <>
      {!isCreated && !isError && (
        <>
          <StyledLink to="../../">메인화면으로 돌아가기</StyledLink>
          <form onSubmit={formSubmissionHandler}>
            <label htmlFor="document-title">문서 제목: </label>
            <input
              id="document-title"
              name="documentTitle"
              type="text"
              value={enteredTitle}
              onChange={(event) => setDocumentTitle(event.target.value)}
            ></input>
            <button type="submit">문서 만들기</button>
          </form>
        </>
      )}
      {isCreated && <CreateResult documentURL={documentURL} />}
      {isError && (
        <Error
          title={"문서생성 실패..."}
          status={error.status}
          message={error.message}
          link={"../../"}
        />
      )}
    </>
  );
};

CreateDocument.propTypes = {
  token: PropTypes.string,
};

export default CreateDocument;
