import PropTypes from "prop-types";
import styled from "styled-components";
import { Link } from "react-router-dom";

const StyledLink = styled(Link)`
  display: block;
`;

const CreateResult = ({ documentURL }) => {
  return (
    <>
      <h1>문서 생성 완료</h1>
      <StyledLink to={`../${documentURL}`}>만든 문서로 가기</StyledLink>
      <StyledLink to="../../">메인화면으로 돌아가기</StyledLink>
    </>
  );
};

CreateResult.propTypes = {
  documentURL: PropTypes.string,
};

export default CreateResult;
